const people = document.getElementById('people');

if (people) {
	people.addEventListener('click', e => {
		if (e.target.className === 'btn btn-danger delete-person') {
			if (confirm('Confirma a exclusão?')) {
				const id = e.target.getAttribute('data-id');

				fetch(`/person/delete/${id}`, {
					method: 'DELETE'
				}).then(res => window.location.reload());
			}
		}
	});
}
