<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonRepository")
 */
class Person
{
    /**
     * @var \DateTime
     * @ORM\Column(name="data_nasc", type="date")
     */
    protected $data_nasc;
    /**
     * @var \DateTime
     * @ORM\Column(name="cadastro", type="date")
     */
    protected $cadastro;
    
    /**
     * @var \DateTime
     * @ORM\Column(name="alteracao", type="date")
     */   
    protected $alteracao;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @ORM\OneToMany(targetEntity="App\Entity\PersonAdress", mappedBy="cod_pessoa")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $nome;

    /**
     * @ORM\Column(type="string", length=11)
     */
    private $cpf;

    /**
     * @ORM\Column(type="string", length=12)
     */
    private $telefone;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $email;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    public function getCpf()
    {
        return $this->cpf;
    }

    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataNasc()
    {
        return $this->data_nasc;
    }

    public function setDataNasc(\DateTime $data_nasc)
    {
        $this->data_nasc = $data_nasc;
        return $this;
    }

    public function getTelefone()
    {
        return $this->telefone;
    }

    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->cadastro;
    }
    public function setDataCadastro(\DateTime $cadastro)
    {
        $this->cadastro = $cadastro;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataAlteracao()
    {
        return $this->alteracao;
    }
    public function setDataAlteracao(\DateTime $alteracao)
    {
        $this->alteracao = $alteracao;
        return $this;
    }
}
