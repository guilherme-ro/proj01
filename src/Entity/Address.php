<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AddressRepository")
 */
class Address
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $endereco;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $numero_endereco;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $complemento;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $bairro;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $cidade;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $uf;

    /**
     * @ORM\Column(name="created_at", type="date")
     */
    private $data_cadastro;
    
    /**
     * @ORM\Column(name="updated_at", type="date")
     */   
    private $data_alteracao;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", inversedBy="id")
     * @ORM\Column(type="integer")
     */
    private $cod_pessoa;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEndereco()
    {
        return $this->endereco;
    }

    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
        return $this;
    }

    public function getNroEndereco()
    {
        return $this->numero_endereco;
    }

    public function setNroEndereco($numero_endereco)
    {
        $this->numero_endereco = $numero_endereco;
        return $this;
    }

    public function getComplemento()
    {
        return $this->complemento;
    }

    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;
        return $this;
    }

    public function getBairro()
    {
        return $this->bairro;
    }

    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
        return $this;
    }

    public function getCidade()
    {
        return $this->cidade;
    }

    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
        return $this;
    }

    public function getUf()
    {
        return $this->uf;
    }

    public function setUf($uf)
    {
        $this->uf = $uf;
        return $this;
    }

    public function getDataCadastro()
    {
        return $this->data_cadastro;
    }
    public function setDataCadastro($data_cadastro)
    {
        $this->data_cadastro = new \DateTime($data_cadastro);
        return $this;
    }

    public function getDataAlteracao()
    {
        return $this->data_alteracao;
    }
    public function setDataAlteracao($data_alteracao)
    {
        $this->data_alteracao = new \DateTime($data_alteracao);
        return $this;
    }

    public function getCodPessoa(): ?int
    {
        return $this->cod_pessoa;
    }

    public function setCodPessoa($cod_pessoa)
    {
        $this->cod_pessoa = $cod_pessoa;
        return $this->cod_pessoa;
    }
}
