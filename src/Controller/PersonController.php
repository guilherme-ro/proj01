<?php
    namespace App\Controller;

    use App\Entity\Person;
    use App\Entity\PersonAdress;

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Routing\Annotation\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\Extension\Core\Type\DateType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;



    class PersonController extends Controller {
        /**
         * @Route("/", name="person_list")
         * @Method({"GET"})
         */
        public function index() {
            $people = $this->getDoctrine()->getRepository(Person::class)->findAll();

            return $this->render('people/index.html.twig', array('people' => $people));
        }

        /**
         * @Route("/person/new", name="new_person")
         * Method({"GET", "POST"})
         */
        public function new(Request $request) {
            $person = new Person();
           

            $form = $this->createFormBuilder($person)
                ->add('nome', TextType::class, array(
                    'label' => 'Nome:', 'attr' =>
                array('class' => 'form-control')))
                ->add('cpf', TextType::class, array(
                    'label' => 'CPF:', 'attr' => 
                array('class' => 'form-control')))
                ->add('data_nasc', DateType::class, array(
                    'label' => 'Data de Nascimento:',
                    'widget'        => 'single_text',
                    ))
                ->add('telefone', TextType::class, array(
                    'label' => 'Telefone:', 'attr' => 
                array('class' => 'form-control')))
                ->add('email', TextType::class, array(
                    'label' => 'Email:','attr' => 
                array('class' => 'form-control')))
                ->add('data_cadastro', DateType::class, array(
                    'label' => 'Data de Cadastro:',
                    'widget'        => 'single_text',
                    ))
                ->add('data_alteracao', DateType::class, array(
                    'label' => 'Última Alteração:',
                    'widget'        => 'single_text',
                    ))
                
                ->add('save', SubmitType::class, array(
                    'label' => 'Cadastrar',
                    'attr' => array('class' => 'btn btn-primary mt-3')
                ))
                ->getForm();

                $form->handleRequest($request);

                if($form->isSubmitted() && $form->isValid()) {
                    $person = $form->getData();

                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($person);
                    $entityManager->flush();

                    return $this->redirectToRoute('person_list');
                }

            return $this->render('people/new.html.twig', array(
                'form' => $form->createView()
            ));
        }

        /**
         * @Route("/person/{id}", name="person_show")
         */
        public function show($id) {
            $person = $this->getDoctrine()->getRepository(Person::class)->find($id);

            return $this->render('people/show.html.twig', array('person' => $person));
        }

        /**
         * @Route("/person/delete/{id}")
         * Method({"DELETE"})
         */
        public function delete(Request $request, $id) {
            $person = $this->getDoctrine()->getRepository
            (Person::class)->find($id);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($person);
            $entityManager->flush();

            $response = new Response();
            $response->send();
        }

        /**
         * @Route("/person/edit/{id}", name="edit_person")
         * Method({"GET", "POST"})
         */
        public function edit(Request $request, $id) {
            $person = new Person();
            $person = $this->getDoctrine()->getRepository
            (Person::class)->find($id);
           

            $form = $this->createFormBuilder($person)
                ->add('nome', TextType::class, array(
                    'label' => 'Nome:', 'attr' =>
                array('class' => 'form-control')))
                ->add('cpf', TextType::class, array(
                    'label' => 'CPF:', 'attr' => 
                array('class' => 'form-control')))
                ->add('data_nasc', DateType::class, array(
                    'label' => 'Data de Nascimento:',
                    'widget'        => 'single_text',
                    ))
                ->add('telefone', TextType::class, array(
                    'label' => 'Telefone:', 'attr' => 
                array('class' => 'form-control')))
                ->add('email', TextType::class, array(
                    'label' => 'Email:','attr' => 
                array('class' => 'form-control')))
                ->add('data_cadastro', DateType::class, array(
                    'label' => 'Data de Cadastro:',
                    'widget'        => 'single_text',
                    ))
                ->add('data_alteracao', DateType::class, array(
                    'label' => 'Última Alteração:',
                    'widget'        => 'single_text',
                    ))
                
                ->add('save', SubmitType::class, array(
                    'label' => 'Concluir',
                    'attr' => array('class' => 'btn btn-primary mt-3')
                ))
                ->getForm();

                $form->handleRequest($request);

                if($form->isSubmitted() && $form->isValid()) {

                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->flush();

                    return $this->redirectToRoute('person_list');
                }

            return $this->render('people/edit.html.twig', array(
                'form' => $form->createView()
            ));
        }
    }

