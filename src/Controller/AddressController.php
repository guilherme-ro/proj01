<?php
    namespace App\Controller;

    use App\Entity\Person;
    use App\Entity\Address;

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Routing\Annotation\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    
    
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\Extension\Core\Type\DateType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\Extension\Core\Type\NumberType;

    class AddressController extends Controller { 
        /**
         * @Method({"GET"})
         */
        public function index() {
            $addresses = $this->getDoctrine()->getRepository(Address::class)->findAll();

            return $this->render('', array('addresses' => $addresses));
        }

        /**
         * @Route("/address/new", name="new_address")
         * Method({"GET", "POST"})
         */
        public function new(Request $request) {
            $address = new Address();
           

            $form_address = $this->createFormBuilder($address)
                ->add('endereco', TextType::class, array(
                    'label' => 'Endereço:', 'attr' =>
                array('class' => 'form-control')))
                ->add('nro_endereco', TextType::class, array(
                    'label' => 'Número:', 'attr' => 
                array('class' => 'form-control')))
                ->add('complemento', TextType::class, array(
                    'label' => 'Complemento:', 'attr' => 
                array('class' => 'form-control')))
                ->add('bairro', TextType::class, array(
                    'label' => 'Bairro:', 'attr' => 
                array('class' => 'form-control')))
                ->add('cidade', TextType::class, array(
                    'label' => 'Cidade:', 'attr' => 
                array('class' => 'form-control')))
                ->add('uf', TextType::class, array(
                    'label' => 'UF:', 'attr' => 
                array('class' => 'form-control')))
                ->add('data_cadastro', DateType::class, array(
                    'label' => 'Data de Cadastro:',
                    'widget'        => 'single_text',
                    ))
                ->add('data_alteracao', DateType::class, array(
                    'label' => 'Última Alteração:',
                    'widget'        => 'single_text',
                    ))
                
                ->add('save', SubmitType::class, array(
                    'label' => 'Cadastrar',
                    'attr' => array('class' => 'btn btn-primary mt-3')
                ))
                ->getForm();

                $form_address->handleRequest($request);

                if($form_address->isSubmitted() && $form_address->isValid()) {
                    $address = $form_address->getData();

                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($address);
                    $entityManager->flush();

                    return $this->redirectToRoute('address_list');
                }

            return $this->render('addresses/new.html.twig', array(
                'form' => $form_address->createView()
            ));
        }

        /**
         * @Route("/address/delete/{id}")
         * Method({"DELETE"})
         */
        public function delete(Request $request, $id) {
            $address = $this->getDoctrine()->getRepository
            (Address::class)->find($id);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($address);
            $entityManager->flush();

            $response = new Response();
            $response->send();
        }

        public function show($id) {
            $address = $this->getDoctrine()->getRepository(Address::class)->find($id);

            return $this->render('addresses/show.html.twig', array('address' => $address));
        }

        /**
         * @Route("/address/edit/{id}", name="edit_address")
         * Method({"GET", "POST"})
         */
        public function edit(Request $request, $id) {
            $address = new Address();
            $address = $this->getDoctrine()->getRepository
            (Address::class)->find($id);
           

            $form_address = $this->createFormBuilder($address)
                ->add('endereco', TextType::class, array(
                    'label' => 'Endereço:', 'attr' =>
                array('class' => 'form-control')))
                ->add('nro_endereco', TextType::class, array(
                    'label' => 'Número:', 'attr' => 
                array('class' => 'form-control')))
                ->add('complemento', TextType::class, array(
                    'label' => 'Complemento:', 'attr' => 
                array('class' => 'form-control')))
                ->add('bairro', TextType::class, array(
                    'label' => 'Bairro:', 'attr' => 
                array('class' => 'form-control')))
                ->add('cidade', TextType::class, array(
                    'label' => 'Cidade:', 'attr' => 
                array('class' => 'form-control')))
                ->add('uf', TextType::class, array(
                    'label' => 'Uf:', 'attr' => 
                array('class' => 'form-control')))
                ->add('data_cadastro', DateType::class, array(
                    'label' => 'Data de Cadastro:',
                    'widget'        => 'single_text',
                    ))
                ->add('data_alteracao', DateType::class, array(
                    'label' => 'Última Alteração:',
                    'widget'        => 'single_text',
                    ))
                
                ->add('save', SubmitType::class, array(
                    'label' => 'Cadastrar',
                    'attr' => array('class' => 'btn btn-primary mt-3')
                ))
                ->getForm();

                $form_address->handleRequest($request);

                if($form_address->isSubmitted() && $form_address->isValid()) {

                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->flush();

                    return $this->redirectToRoute('address_list');
                }

            return $this->render('address/edit.html.twig', array(
                'form' => $form_address->createView()
            ));
        }

    }