<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190814092430 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, endereco VARCHAR(60) NOT NULL, numero_endereco VARCHAR(15) NOT NULL, complemento VARCHAR(15) NOT NULL, bairro VARCHAR(15) NOT NULL, cidade VARCHAR(45) NOT NULL, uf VARCHAR(2) NOT NULL, created_at DATE NOT NULL, updated_at DATE NOT NULL, cod_pessoa INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE person_adress');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE person_adress (id INT AUTO_INCREMENT NOT NULL, endereco VARCHAR(60) NOT NULL COLLATE utf8mb4_unicode_ci, numero_endereco VARCHAR(15) NOT NULL COLLATE utf8mb4_unicode_ci, complemento VARCHAR(15) NOT NULL COLLATE utf8mb4_unicode_ci, bairro VARCHAR(15) NOT NULL COLLATE utf8mb4_unicode_ci, cidade VARCHAR(45) NOT NULL COLLATE utf8mb4_unicode_ci, uf VARCHAR(2) NOT NULL COLLATE utf8mb4_unicode_ci, created_at DATE NOT NULL, updated_at DATE NOT NULL, cod_pessoa INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE address');
    }
}
