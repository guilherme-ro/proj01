<?php

namespace App\Repository;

use App\Entity\PersonAdress;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PersonAdress|null find($id, $lockMode = null, $lockVersion = null)
 * @method PersonAdress|null findOneBy(array $criteria, array $orderBy = null)
 * @method PersonAdress[]    findAll()
 * @method PersonAdress[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonAdressRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PersonAdress::class);
    }

    // /**
    //  * @return PersonAdress[] Returns an array of PersonAdress objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PersonAdress
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
